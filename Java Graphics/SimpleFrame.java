import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.Color;
import javax.swing.*;

/**
 * CSCU9N6 - Practical 1
 * SimpleFrame.java
 * 
 * @author Michael Sammels
 * @version 31.01.2022
 */

public class SimpleFrame extends JFrame {
    public static void main(String[] args) {
        SimpleFrame sf = new SimpleFrame();
        sf.go();
        sf.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
    
    public void go() {
        setSize(800, 600);
        setVisible(true);
    }
    
    public void paint(Graphics g) {
        if (g instanceof Graphics2D) {
            Graphics2D g2D = (Graphics2D)g;
            
            g2D.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
                    RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            g2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                    RenderingHints.VALUE_ANTIALIAS_ON);
        }
        
        g.setColor(Color.blue);
        g.drawString("Hello World!", 20, 50);
        g.drawOval(20, 100, 30, 40);
        
        g.setColor(Color.red);
        g.drawString("Line", 200, 50);
        g.drawLine(280, 80, 120, 80);
        
        g.setColor(Color.green);
        g.drawString("Arc", 350, 50);
        g.drawArc(350, 100, 40, 40, 45, 135);
    }
}
