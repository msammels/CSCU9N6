<!-- GitHub Project README -->

<!-- PROJECT SHIELDS -->
[![Contributors][contributors-shield]][contributors-url]
[![Forks][forks-shield]][forks-url]
[![Stargazers][stars-shield]][stars-url]
[![The Unlicense][license-shield]][license-url]
[![Issues][issues-shield]][issues-url]
[![LinkedIn][linkedin-shield]][linkedin-url]

<!-- PROJECT LOGO -->
<br />
<div align="center">
	<a href="https://github.com/msammels/CSCU9N6">
		<img src="images/primary-logo.png" alt="University of Stirling Logo" height="80">
	</a>
	<h3 align="center">CSCU9N6</h3>
	<p align="center">
		Computer Games Development
		<br />
		<a href="https://github.com/msammels/CSCU9N6"><strong>Explore the code »</strong></a>
	</p>
</div>

<!-- TABLE OF CONTENTS -->
<details>
	<summary>Table of Contents</summary>
	<ol>
		<li>
			<a href="#about-the-project">About The Project</a>
			<ul><li><a href=""#built-with">Built With</a></li></ul>
		</li>
		<li>
			<a href="#getting-started">Getting Started</a>
			<ul><li><a href="#prerequisites">Prerequisites</a></li></ul>
			<ul><li><a href="#folder-layout">Folder Layout</a></li></ul>
		</li>
		<li><a href="#contact">Contact</a></li>
	</ol>
</details>

<!-- ABOUT THE PROJECT -->
## About the Project
The code within this repository was designed and developed in line with the practical classes for the third year
course at the University of Stirling.

<p align="right">(<a href="#top">Back to top</a>)</p>

## Built With
* [Java SDK](https://www.oracle.com/java/technologies/downloads/)
* [Eclipse](https://www.eclipse.org)

<p align="right">(<a href="#top">Back to top</a>)</p>

<!-- GETTING STARTED -->
## Getting Started
As stated earlier, the code was created using my favourite editor, but I have removed all of the files that the
editor generated, therefore leaving only the core Java files.

## Prerequisites
The code here does not use any external libraries such as [JUnit](https://junit.org/junit5/), nor does it require
[Maven](https://maven.apache.org) or [Gradle](https://gradle.org) to run, so you can use your editor of choice.

## Folder Layout
You will notice this does follow the standard folder structure, as normally found:

```
src/main/java/com/msammels/File.java
src/test/java/com/msammels/File.java
```

You should be able to move things into whatever folder structure that you feel is most appropriate and things
should still work without many issues. If you are using [IntelliJ IDEA](https://www.jetbrains.com/idea/) then
you will need to make sure that each folder is located within it's own module (so to avoid conflicts when compiling)
or, alternatively make each one a separate project.

<!-- CONTACT -->
## Contact
Michael Sammels - [@msammels](https://twitter.com/msammels) | msa@cs.stir.ac.uk

<!-- MARKDOWN LINKS & IMAGES -->
[contributors-shield]:  https://img.shields.io/github/contributors/msammels/CSCU9N6?style=for-the-badge
[contributors-url]:     https://github.com/msammels/CSCU9N6/graphs/contributors

[forks-shield]:         https://img.shields.io/github/forks/msammels/CSCU9N6?style=for-the-badge
[forks-url]:            https://github.com/msammles/CSCU9N6/network/members

[stars-shield]:         https://img.shields.io/github/stars/msammels/CSCU9N6?style=for-the-badge
[stars-url]:            https://github.com/msammels/CSCU9N6/stargazers

[issues-shield]:        https://img.shields.io/github/issues/msammels/CSCU9N6?style=for-the-badge
[issues-url]:           https://github.com/msammels/CSCU9N6/issues

[license-shield]:		https://img.shields.io/github/license/msammels/CSCU9N6.svg?style=for-the-badge
[license-url]:			https://github.com/msammels/CSCU9N6/blob/main/LICENSE

[linkedin-shield]:      https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]:         https://linkedin.com/in/msammels
