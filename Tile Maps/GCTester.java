import java.awt.*;
import game2D.*;
import java.awt.event.*;
import java.awt.geom.*;

/**
 * CSCU9N6 - Practical 4
 * GCTester.java
 * 
 * GCTester demonstrates how we can override the GameCore class to create our own 'game'. We usually need to implement at
 * least the 'draw' and 'update' (not including any local event handling) to begin the process. You should also write your
 * own 'init' method that will initialise event handlers etc. By default GameCore will handle the 'Escape' key to quit
 * the game but you can override this with your own event handler
 * 
 * @author Michael Sammels
 * @version 11.02.2022
 */

public class GCTester extends GameCore {
    long total;                     // Total time elapsed
    Animation anim;                 // Our animation
    Sprite boulder, rock;           // Our sprites
    TileMap tmap = new TileMap();   // Our tile map
    Boolean paused = false;         // The status of the game loop
    private double angle = 45;      // The angle of rotation
    
    /**
     * The obligatory main method that creates an instance of our GCTester class and starts it running
     * @param args 
     */
    public static void main(String[] args) {
        GCTester gct = new GCTester();
        gct.init();
        
        // Start in windowed mode with an 800x600 screen
        gct.run(false, 768, 576);
    }
    
    /**
     * Initialise the class, e.g., set up variables, animations, register event handlers
     */
    public void init() {
        total = 0;
        anim = new Animation();
        anim.addFrame(loadImage("images/rock.png"), 250);
        
        rock = new Sprite(anim);
        rock.setPosition(70, 70);
        rock.setVelocity(0.1f, 0.1f);
        
        boulder = new Sprite(anim);
        boulder.setPosition(500, 500);
        boulder.setVelocity(-0.1f, -0.1f);
        
        angle = 90;
        
        tmap.loadMap("maps", "example-map.txt");
    }
    
    /**
     * Draw the current frame
     * @param g 
     */
    @Override
    public void draw(Graphics2D g) {
        // A simple demo - note that is not very efficient since we fill the screen on every frame
        g.setColor(Color.black);
        g.fillRect(0, 0, getWidth(), getHeight());
        g.setColor(Color.yellow);
        g.drawString("Time Expired: " + total, 30, 50);
        
        // Checking where the bounding box of each sprite is
        g.setColor(Color.yellow);
        boulder.drawBoundingBox(g);
        rock.drawBoundingBox(g);
        
        AffineTransform transform = new AffineTransform();
        transform.translate(Math.round(rock.getX()), Math.round(rock.getY()));
        transform.translate(Math.round(boulder.getX()), Math.round(boulder.getY()));

        rock.draw(g); boulder.draw(g); tmap.draw(g, 0, 0);
    }
    
    /**
     * Update any sprites and check for collisions
     * @param elapsed 
     */
    @Override
    public void update(long elapsed) {
        if (paused) return;
        total += elapsed;
        rock.update(elapsed);
        boulder.update(elapsed);
        
        AffineTransform transform = new AffineTransform();
        transform.translate(Math.round(rock.getX()), Math.round(rock.getY()));
        transform.translate(Math.round(boulder.getX()), Math.round(boulder.getY()));
        transform.scale(2.5f, 2.5f);
        transform.rotate(Math.toRadians(angle = (angle + 5) % 360));
        
        checkScreenEdge(rock); checkScreenEdge(boulder);
        
        // Collision detection for the rock and boulder
        if (boundingBoxCollision(rock, boulder)) {
            rock.setVelocityX(-rock.getVelocityX());
            boulder.setVelocityX(-boulder.getVelocityX());
        }
        
        // Collision detection for the sprites to the tile map
        checkTileCollision(rock, tmap); checkTileCollision(boulder, tmap);
    }
    
    /**
     * The event handlers for key press detection
     * @param e 
     */
    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_ESCAPE -> stop();                      // Stop game loop
            case KeyEvent.VK_S      -> rock.stop();                 // Stop the rock
            case KeyEvent.VK_G      -> paused = !paused;            // Pause and unpause the game loop
            case KeyEvent.VK_RIGHT  -> angle = (angle + 5) % 360;   // Rotate right
            case KeyEvent.VK_LEFT   -> angle = (angle - 5) % 360;   // Rotate left
            
            // Unused key event
            default -> {}
        }
        e.consume();
    }
    
    /**
     * Basic collision detection (for the edge of the screen)
     * @param s 
     */
    public void checkScreenEdge(Sprite s) {
        if (s.getX() > getWidth())  { s.setX(0); }
        if (s.getY() > getHeight()) { s.setY(0); }
        
        if (s.getX() > getHeight()) { s.setX(0); }
        if (s.getY() > getWidth())  { s.setY(0); }
    }
    
    /**
     * Bounding box collision detection for sprites
     * @param s1
     * @param s2
     * @return 
     */
    public boolean boundingBoxCollision(Sprite s1, Sprite s2) {
        return ((s1.getX() + s1.getImage().getWidth(null) > s2.getX()) && 
                (s1.getX() < (s2.getX() + s2.getImage().getWidth(null))) &&
                ((s1.getY() + s1.getImage().getHeight(null) > s2.getY()) && 
                (s1.getY() < s2.getY() + s2.getImage().getHeight(null))));
    }
    
    /**
     * Collision detection for the tile map objects
     * @param s
     * @param tmap 
     */
    public void checkTileCollision(Sprite s, TileMap tmap) {
        // Take a note of the sprite's current position
        float sx = s.getX();
        float sy = s.getY();
        
        // Find out how wide and tall a tile is
        float tileWidth = tmap.getTileWidth();
        float tileHeight = tmap.getTileHeight();
        
        /*
        Divide the sprite's x coordinate by the width of a tile, to get the number of tiles across the x axis that
        the sprite is at
        */
        int xtile = (int)(sx / tileWidth);
        
        // The same applies to the y coordinate
        int ytile = (int)(sy / tileHeight);
        
        // What tile character is at the top left of the sprite s?
        char ch = tmap.getTileChar(xtile, ytile);
        
        // If it's not a dot (empty space), handle it
        if (ch != '.')  {
            /*
            This reserves the sprites velocity where the collision was detected. We could have simply stopped or hid
            it instead, but this provides a more realistic feel. We will however be removing each tile it hit
            */
            s.setVelocityX(-s.getVelocityX());      // Reverse velocity
            tmap.setTileChar('.', xtile, ytile);    // Remove the tile we hit
        }
    }
}