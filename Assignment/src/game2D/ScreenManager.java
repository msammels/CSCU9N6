package game2D;

import java.awt.*;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JFrame;

/**
 * The ScreenManager class manages initialising and displaying full screen graphics modes
 * 
 * @author	David Cairns
 * @author	Michael Sammels
 * @version	24.02.2022
 */

public class ScreenManager {
	private GraphicsDevice device;
	
	/**
	 * Creates a new ScreenManager object
	 */
	public ScreenManager() {
		GraphicsEnvironment environment = GraphicsEnvironment.getLocalGraphicsEnvironment();
		device = environment.getDefaultScreenDevice();
	}
	
	/**
	 * Returns a list of compatible display modes for the default device on the system
	 * @return	Compatible display modes
	 */
	public DisplayMode[] getCompatibleDisplayModes() {
		return device.getDisplayModes();
	}
	
	/**
	 * Returns the first compatible mode in a list of modes, otherwise returns null if no modes are compatible
	 * @param modes	The modes to locate
	 * @return		The first compatible mode
	 */
	public DisplayMode findFirstCompatibleMode(DisplayMode modes[]) {
		DisplayMode goodModes[] = device.getDisplayModes();
		
		for (int i = 0; i < modes.length; i++) {
			for (int j = 0; j < goodModes.length; j++) {
				if (displayModesMatch(modes[i], goodModes[j])) {
					return modes[i];
				}
			}
		}
		return null;
	}
	
	/**
	 * Returns the current display mode
	 * @return	The display mode to return
	 */
	public DisplayMode getCurrentDisplayMode() {
		return device.getDisplayMode();
	}
	
	/**
	 * Determines if two display modes "match". Two display modes match if they have the same resolution,
	 * bit depth and refresh rate. The bit depth is ignored if one of the modes has a bit depth of
	 * DisplayMode.BIT_DEPTH_MULTI. Likewise, the refresh rate is ignored if one of the modes has a refresh
	 * rate of DisplayMode.REFRESH_RATE_UNKNOWN
	 * @param mode1	The first display mode
	 * @param mode2	The second display mode
	 * @return		True if they match, false otherwise
	 */
	public boolean displayModesMatch(DisplayMode mode1, DisplayMode mode2) {
		if (mode1.getWidth() != mode2.getWidth() || mode1.getHeight() != mode2.getHeight()) {
			return false;
		}
		
		if (mode1.getBitDepth() != DisplayMode.BIT_DEPTH_MULTI && 
				mode2.getBitDepth() != DisplayMode.BIT_DEPTH_MULTI &&
				mode1.getBitDepth() != mode2.getBitDepth()) {
			return false;
		}
		
		if (mode1.getRefreshRate() != DisplayMode.REFRESH_RATE_UNKNOWN &&
				mode2.getRefreshRate() != DisplayMode.REFRESH_RATE_UNKNOWN &&
				mode1.getRefreshRate() != mode2.getRefreshRate()) {
			return false;
		}
		return true;
	}
	
	/**
	 * Enters full screen mode and changes the display mode. If the specified display mode is null or not
	 * compatible with this device, or if the display mode cannot be changed on this system, the current
	 * display mode is used. The display uses a BufferStrategy with 2 buffers
	 * @param displayMode	The mode to switch to
	 */
	public void setFullScreen(DisplayMode displayMode) {
		final JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setUndecorated(true);
		frame.setIgnoreRepaint(true);
		frame.setResizable(false);
		
		device.setFullScreenWindow(frame);
		
		if (displayMode != null && device.isDisplayChangeSupported()) {
			try {
				device.setDisplayMode(displayMode);
			} catch (IllegalArgumentException ex) {}
			
			// Fix for Mac OS X and macOS
			frame.setSize(displayMode.getWidth(), displayMode.getHeight());
		}
		
		// Avoid potential deadlock in 1.4.1_02
		try {
			EventQueue.invokeAndWait(new Runnable() {
				public void run() {
					frame.createBufferStrategy(2);
				}
			});
		} catch (InterruptedException ex) { /* Do nothing, for now */ }
		catch (InvocationTargetException ex) { /* Do nothing, for now */ }
	}
	
	/**
	 * Gets the graphics context for the display. The ScreenManager uses double buffering, so applications
	 * must call update() to show any graphics drawn. he application must dispose of the graphics object
	 * @return	The graphics context
	 */
	public Graphics2D getGraphics() {
		Window window = device.getFullScreenWindow();
		
		if (window != null) {
			BufferStrategy strategy = window.getBufferStrategy();
			return (Graphics2D)strategy.getDrawGraphics();
		} else {
			return null;
		}
	}
	
	/**
	 * Updates the display
	 */
	public void update() {
		Window window = device.getFullScreenWindow();
		
		if (window != null) {
			BufferStrategy strategy = window.getBufferStrategy();
			
			if (!strategy.contentsLost()) {
				strategy.show();
			}
		}
		
		// Sync the display on some systems. On Linux, this fixes event queue problems
		Toolkit.getDefaultToolkit().sync();
	}
	
	/**
	 * Returns the window currently used in full screen mode
	 * @return	The window currently used, or null if the device is not in full screen mode
	 */
	public JFrame getFullScreenWindow() {
		return (JFrame)device.getFullScreenWindow();
	}
	
	/**
	 * Returns the width of the window currently used in full screen mode
	 * @return	The width of the current window, otherwise null if the device is not used in full screen mode
	 */
	public int getWidth() {
		Window window = device.getFullScreenWindow();
		
		if (window != null) {
			return window.getWidth();
		} else {
			return 0;
		}
	}
	
	public int getHeight() {
		Window window = device.getFullScreenWindow();
		
		if (window != null) {
			return window.getHeight();
		} else {
			return 0;
		}
	}
	
	/**
	 * Restores the screen's display mode
	 */
	public void restoreScreen() {
		Window window = device.getFullScreenWindow();
		
		if (window != null) {
			window.dispose();
		}
		device.setFullScreenWindow(null);
	}
	
	/**
	 * Creates an image compatible with the current display
	 * @param w				The width of the image
	 * @param h				The height of the image
	 * @param transparency	The transparency of the image
	 * @return				The compatible image
	 */
	public BufferedImage createCompatibleImage(int w, int h, int transparency) {
		Window window = device.getFullScreenWindow();
		
		if (window != null) {
			GraphicsConfiguration gc = window.getGraphicsConfiguration();
			return gc.createCompatibleImage(w, h, transparency);
		}
		return null;
	}
}