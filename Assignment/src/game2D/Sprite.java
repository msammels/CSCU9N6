package game2D;

import java.awt.Image;
import java.awt.*;
import java.awt.geom.*;

/**
 * This class provides the functionality for a moving animated image or sprite
 * 
 * @author	David Cairns
 * @author	Michael Sammels
 * @version	16.03.2022
 */

public class Sprite {
	// The current animation to use for this sprite
	private Animation anim;
	
	// Position (pixels)
	private float x;
	private float y;
	
	// Velocity (pixels per millisecond)
	private float dx;
	private float dy;
	
	// Dimensions of the sprite
	private float height;
	private float width;
	private float radius;
	
	// The scale to draw the sprite at, where 1 equals normal size
	private double xscale;
	private double yscale;
	
	// The rotation to apply to the sprite image
	private double rotation;
	
	// If render is 'true', the sprite will be drawn when requested
	private boolean render;
	
	/*
	 * The draw offset associated with this sprite. Used to draw it relative to specific on screen position 
	 * (usually the player) 
	 */
	private int xoff = 0;
	private int yoff = 0;
	
	/**
	 * Creates a new Sprite object with the specified animation
	 * @param anim	The animation to use for the sprite
	 */
	public Sprite(Animation anim) {
		this.anim = anim;
		render = true;
		xscale = 1.0f;
		yscale = 1.0f;
		rotation = 0.0f;
	}
	
	/**
	 * Change the animation for the sprite to {@code a}
	 * @param a	The animation to use for the sprite
	 */
	public void setAnimation(Animation a) {
		anim = a;
	}
	
	/**
	 * Set the current animation to the given {@code frame}
	 * @param frame	The frame to set the animation to
	 */
	public void setAnimationFrame(int frame) {
		anim.setAnimationFrame(frame);
	}
	
	/**
	 * Pauses the animation at its current frame. Note that the sprite will continue to move, it just won't animate
	 */
	public void pauseAnimation() {
		anim.pause();
	}
	
	/**
	 * Pause the animation when it reaches frame {@code f}
	 * @param f	The frame to stop the animation at
	 */
	public void pauseAnimationAtFrame(int f) {
		anim.pauseAt(f);
	}
	
	/**
	 * Change the speed at which the current animation runs. A speed of 1 will result in a normal animation,
	 * 0.6 will be half the normal rate and 2 will double it
	 * @apiNote 	If you change the animation, it will run at whatever speed it was previously set to
	 * @param speed	The speed to set the current animation to
	 */
	public void setAnimationSpeed(float speed) {
		anim.setAnimationSpeed(speed);
	}
	
	/**
	 * Starts an animation playing if it has been paused
	 */
	public void playAnimation() {
		anim.play();
	}
	
	/**
	 * Returns a reference to the current animation assigned to this sprite
	 * @return	A reference to the current animation
	 */
	public Animation getAnimation() {
		return anim;
	}
	
	/**
	 * Updates this sprite's animation and its position based on the elapsed time
	 * @param elapsedTime	The time that has elapsed since the last call to update
	 */
	public void update(long elapsedTime) {
		if (!render) return;
		
		x += dx * elapsedTime;
		y += dy * elapsedTime;
		anim.update(elapsedTime);
		width = getWidth();
		height = getHeight();
		
		if (width > height)
			radius = width / 2.0f;
		else
			radius = height / 2.0f;
	}
	
	/**
	 * Gets this sprite's current x position
	 * @return	The current x position
	 */
	public float getX() {
		return x;
	}
	
	/**
	 * Gets this sprite's current y position
	 * @return	The current y position
	 */
	public float getY() {
		return y;
	}
	
	/**
	 * Sets this sprite's current {@code x} position
	 * @param x	The position to set
	 */
	public void setX(float x) {
		this.x = x;
	}
	
	/**
	 * Sets this sprite's current {@code y} position
	 * @param y	The position to set
	 */
	public void setY(float y) {
		this.y = y;
	}
	
	/**
	 * Sets this sprite's new {@code x} and {@code y} position
	 * @param x	The x coordinate to set
	 * @param y	The y coordinate to set
	 */
	public void setPosition(float x, float y) {
		setX(x);
		setY(y);
	}
	
	/**
	 * Shifts the x position
	 * @param shift	How far to shift in x direction
	 */
	public void shiftX(float shift) {
		this.x += shift;
	}
	
	/**
	 * Shifts the y position
	 * @param shift	How far to shift in the y direction
	 */
	public void shiftY(float shift) {
		this.y += shift;
	}
	
	/**
	 * Gets this sprite's width, based on the size of the current image
	 * @return	The sprite's width
	 */
	public int getWidth() {
		return (int)(anim.getImage().getWidth(null) * Math.abs(xscale));
	}
	
	/**
	 * Gets this sprite's height, based on the size of the current image
	 * @return	The sprite's height
	 */
	public int getHeight() {
		return (int)(anim.getImage().getHeight(null) * Math.abs(yscale));
	}
	
	/**
	 * Gets the sprite's radius in pixels
	 * @return	The radius (px)
	 */
	public float getRadius() {
		return radius;
	}
	
	/**
	 * Gets the horizontal velocity of this sprite in pixels per millisecond
	 * @return	The horizontal velocity
	 */
	public float getVelocityX() {
		return dx;
	}
	
	/**
	 * Gets the vertical velocity of this sprite in pixels per millisecond
	 * @return	The vertical velocity
	 */
	public float getVelocityY() {
		return dy;
	}
	
	/**
	 * Sets the horizontal velocity of this sprite in pixels per millisecond
	 * @param dx	The horizontal velocity to set
	 */
	public void setVelocityX(float dx) {
		this.dx = dx;
	}
	
	/**
	 * Sets the vertical velocity of this sprite in pixels per millisecond
	 * @param dy	The vertical velocity to set
	 */
	public void setVelocityY(float dy) {
		this.dy = dy;
	}
	
	/**
	 * Sets the horizontal and vertical velocity of this sprite in pixels per millisecond
	 * @param dx	The horizontal velocity
	 * @param dy	The vertical velocity
	 */
	public void setVelocity(float dx, float dy) {
		this.dx = dx;
		this.dy = dy;
	}
	
	/**
	 * Set the x and y scale of the sprite to {@code scx} and {@code scy} respectively. If {@code scx} and
	 * {@code sxy} are 1, the sprite will be drawn at normal size. If they are 0.5, it will be drawn at half size.
	 * If {@code scx} is -1, the sprite will be flipped along its vertical axis (it will face left instead of
	 * right). Negative values of {@code scy} will flip along the horizontal axis. The flipping and scaling of
	 * the sprite are now accounted for when setting a sprite position and getting its width and height (you will
	 * always reference the top left of the sprite irrespective of the scaling)
	 * @apiNote		Scaling and rotation are only applied when using the {@link Sprite#drawTransformed} method
	 * @param scx	The x scale to set
	 * @param scy	The y scale to set
	 */
	public void setScale(float scx, float scy) {
		xscale = scx;
		yscale = scy;
	}
	
	/**
	 * Set the scale of the sprite to {@code s}. If s is 1, the sprite will be drawn at normal size. If s is 0.5,
	 * it will be drawn at half size
	 * @apiNote	Scaling and rotation are only applied when using the {@link drawTransformed} method
	 * @param s	The scale to set
	 */
	public void setScale(float s) {
		xscale = s;
		yscale = s;
	}
	
	/**
	 * Get the current value of the x scaling attribute. 
	 * @see		{@link Sprite#setScale} for more information
	 * @return	The current value of the x scale
	 */
	public double getScaleX() {
		return xscale;
	}
	
	/**
	 * Get the current value of the y scaling attribute. See {@link Sprite#setScale} for more information
	 * @return	The current value of the y scale
	 */
	public double getScaleY() {
		return yscale;
	}
	
	/**
	 * Set the rotation angle for the sprite in degrees
	 * @apiNote	Scaling and rotation are only applied when using the {@link Sprite#drawTransformed} method
	 * @param r	The rotation
	 */
	public void setRotation(double r) {
		rotation = Math.toRadians(r);
	}
	
	/**
	 * Get the current value of the rotation attribute in degrees
	 * @see		setRotation(double r) for more information
	 * @return	The current rotation attribute
	 */
	public double getRotation() {
		return Math.toDegrees(rotation);
	}
	
	/**
	 * Stops the sprite's movement at the current position
	 */
	public void stop() {
		dx = 0;
		dy = 0;
	}
	
	/**
	 * Gets this sprite's current image
	 */
	public Image getImage() {
		return anim.getImage();
	}
	
	/**
	 * Draws the sprite with the graphics object 'g' at the current x and y coordinates. Scaling and rotation
	 * transforms are NOT applied
	 * @param g	The graphics object
	 */
	public void draw(Graphics2D g) {
		if (!render) return;
		
		g.drawImage(getImage(), (int)x + xoff, (int)y + yoff, null);
	}
	
	/**
	 * Draws the bounding box of this sprite using the graphics object 'g' and the currently selected 
	 * foreground colour
	 * @param g	The graphics object
	 */
	public void drawBoundingBox(Graphics2D g) {
		if (!render) return;
		
		Image img = getImage();
		g.drawRect((int)x, (int)y, img.getWidth(null), img.getHeight(null));
	}
	
	/**
	 * Draws the bounding circle of this sprite using the graphics object 'g; and the current selected
	 * foreground colour
	 * @param g	The graphics object
	 */
	public void drawBoundingCircle(Graphics2D g) {
		if (!render) return;
		
		Image img = getImage();
		g.drawArc((int)x, (int)y, img.getWidth(null), img.getHeight(null), 0, 360);
	}
	
	/**
	 * Draws the sprite with the graphics object 'g' at the current x and y coordinates with the current
	 * scaling and rotation transforms applied
	 * @param g	The graphics object to draw to
	 */
	public void drawTransformed(Graphics2D g) {
		if (!render) return;
		
		AffineTransform transform = new AffineTransform();
		
		// Apply scaling to the current x and y positions to ensure shifted left and up when flipped due to scaling
		float shiftx = 0;
		float shifty = 0;
		
		if (xscale < 0) shiftx = getWidth();
		if (yscale < 0) shifty = getHeight();
		
		transform.translate(Math.round(x) + shiftx + xoff,  Math.round(y) + shifty + yoff);
		transform.scale(xscale, yscale);
		transform.rotate(rotation, getImage().getWidth(null) / 2, getImage().getHeight(null) / 2);
		
		// Apply the transform to the image and draw it
		g.drawImage(getImage(), transform, null);
	}
	
	/**
	 * Hide the sprite
	 */
	public void hide() {
		render = false;
	}
	
	/**
	 * Show the sprite
	 */
	public void show() {
		render = true;
	}
	
	/**
	 * Check the visibility status of the sprite
	 * @return	True if visible, false otherwise
	 */
	public boolean isVisible() {
		return render;
	}
	
	/**
	 * Sets an x & y offset to use when drawing the sprite
	 * @apiNote	This does not affect its actual position, just moves the drawn position
	 * @param x	The x offset
	 * @param y	The y offset
	 */
	public void setOffsets(int x, int y) {
		xoff = x;
		yoff = y;
	}
}
