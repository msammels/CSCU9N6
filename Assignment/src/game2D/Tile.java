package game2D;

/**
 * A Tile in the Tile Map
 * 
 * @author	David Cairns
 * @author	Michael Sammels
 * @version	24.02.2022
 */

public class Tile {
	private char character = ' ';	// The character associated with this tile
	private int xc = 0;				// The tile's x coordinate in pixels
	private int yc = 0;				// The tile's y coordinate in pixels
	
	/**
	 * Create an instance of a tile
	 * @param c	The character associated with this tile
	 * @param x	The x tile coordinate in pixels
	 * @param y	The y tile coordinate in pixels
	 */
	public Tile(char c, int x, int y) {
		character = c;
		xc = x;
		yc = y;
	}
	
	/**
	 * Returns a character
	 * @return	The character for this tile
	 */
	public char getCharacter() {
		return character;
	}
	
	/**
	 * Sets the character
	 * @param character	The character to set the tile to
	 */
	public void setCharacter(char character) {
		this.character = character;
	}
	
	/**
	 * Get the x coordinate in pixels
	 * @return	The x coordinate (pixels)
	 */
	public int getXC() {
		return xc;
	}
	
	/**
	 * Get the y coordinate in pixels
	 * @return	The y coordinate (pixels)
	 */
	public int getYC() {
		return yc;
	}
}
