package core;

import java.awt.*;
import java.awt.event.KeyEvent;

import game2D.*;

/**
 * Game demonstrates how we can override the {@link GameCore} class to create our own 'game'. We usually need to
 * implement at least {@link GameCore#draw()} and {@link GameCore#update()} (not including any local event handling)
 * to begin the process. You should also add code to the {@link GameCore#init()} method that will initialise event
 * handlers, etc. By default, {@link GameCore} will handle the 'Escape' key to quit the game, but you should override
 * this with your own event handler
 * 
 * @author	David Cairns
 * @author	Michael Sammels
 * @version	24.02.2022
 */

public class Game extends GameCore {
	private static final long serialVersionUID = -6194364324346153328L;
	
	// Useful game constants
	static int screenWidth = 512;
	static int screenHeight = 384;
		
	float lift = 0.005f;
	float gravity = 0.005f;
	
	// Game screen constants
	int menuScreen = 0;
	int playScreen = 1;
	int endScreen = 2;
	
	// Game state flags
	boolean falling = false;
	boolean walkLeft = false;
	boolean walkRight = false;
		
	// Sprites and animations
	Animation playerWalking;
	Sprite player;
	
	// Audio and images
	Sound backgroundMusic;

	// Game maps
	TileMap tmap = new TileMap();	// Loaded in the init() method
	
	/**
	 * The obligatory main method that creates an instance of our class and starts it running
	 * @param args	The list of parameters that this program might use (ignored)
	 */
	public static void main(String[] args) {
		Game game = new Game();
		game.init();
		
		// Start in windowed mode, with the given screen height and width
		game.run(false, screenWidth, screenHeight);
	}
	
	/**
	 * Initialises the class, e.g. sets up variables, loads images, creates animations, registers event handlers
	 */
	public void init() {
		playerWalking = new Animation();
		playerWalking.loadAnimationFromSheet("images/player/p1_walk.png", 11, 1, 60);
		
		player = new Sprite(playerWalking);

		initialiseGame();
	}
	
	/**
	 * You will probably want to put code to restart a game in a separate method so that you can call it to
	 * restart the game
	 */
	public void initialiseGame() {
		tmap.loadMap("maps", "level1.txt");	// Load the tile map
		setSize(tmap.getPixelWidth() / 4, tmap.getPixelHeight());
		
		player.setX(20);
		player.setY(100);
		player.setVelocityX(0);
		player.setVelocityY(0);
		player.show();
		
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	/**
	 * This method will helps us determine which audio to play during the course of the game's execution
	 */
	public void audioHandler() {
		backgroundMusic = new Sound("sounds/menu.wav");
		backgroundMusic.start();
	}
	
	/**
	 * Draw the current state of the game
	 * @param g	The graphics device
	 */
	public void draw(Graphics2D g) {
		/*
		 * Be careful about the order in which you draw objects - you should draw the background first, then
		 * work your way 'forward'
		 */
		
		int xo = screenWidth / 2 - Math.round(player.getX());
		int yo = screenHeight / 2 - Math.round(player.getY());
		
		xo = Math.min(xo, 0);
		xo = Math.max(screenWidth - tmap.getPixelWidth(), xo);
		
		yo = Math.min(yo, 0);
		yo = Math.max(screenHeight - tmap.getPixelHeight(), yo);
		
		// Apply offsets to the player and draw
		player.setOffsets(xo, yo);
		player.draw(g);
		
		// Apply offsets to the tile map and draw it
		tmap.draw(g, xo, yo);
	}
	
	/**
	 * Update any sprites and check for collisions
	 * @param elapsed	The elapsed time between this call and the previous call of elapsed
	 */
	public void update(long elapsed) {
		if (!walkLeft || !walkRight) {
			player.pauseAnimationAtFrame(1);
			player.setVelocity(0, 0);
		}
		
		if (walkRight) {
			player.playAnimation();
			player.setAnimationSpeed(1.8f);
			player.setVelocityX(0.2f);
		}
		
		if (walkLeft) {
			player.playAnimation();
			player.setAnimationSpeed(1.8f);
			player.setVelocityX(-0.2f);
		}
		
		if (falling) {
			player.setVelocityY(0.2f);
		}

		// Now update the sprites animation and position
		player.update(elapsed);
		
		// Then check for any collisions that may have occurred
		handleScreenEdge(player, tmap, elapsed);
		checkTileCollision(player, tmap);
	}
	
	/**
	 * Checks and handles collisions with the edge of the screen
	 * @param s			The sprite to check collisions for
	 * @param tmap		The tile map to check
	 * @param elapsed	How much time has gone by since the last call
	 */
	public void handleScreenEdge(Sprite s, TileMap tmap, long elapsed) {
		/*
		 * This method just checks if the sprite has gone off the bottom. Ideally you should use
		 * tile collision instead of this approach
		 */
		if (s.getY() + s.getHeight() > tmap.getPixelHeight()) {
			// Put the player on the map, 1 pixel above the bottom
			s.setY(tmap.getPixelHeight() - s.getHeight() - 1);
			
			// And make them bounce
			s.setVelocityY(-s.getVelocityY());
		}
	}
	
	/**
	 * Checks and handles collisions with a tile map for the given sprite {@code s}. Initially functionality is
	 * limited
	 * @param s		The sprite to check collisions for
	 * @param tmap	The tile map to check
	 */
	public void checkTileCollision(Sprite s, TileMap tmap) {
		// Take a note of a sprite's current position
		float sx = s.getX();
		float sy = s.getY();
		
		// Find out how wide and tall a tile is
		float tileWidth = tmap.getTileWidth();
		float tileHeight = tmap.getTileHeight();
		
		/*
		 * Divide the sprite's x coordinate by the width of a tile, to get the number of tiles across the
		 * x axis that the sprite is positioned at
		 */
		int xtile = (int)(sx / tileWidth);
		
		// The same applies to the y coordinate
		int ytile = (int)(sy / tileHeight);
		
		// What tile character is at the top left of the sprite s?
		char ch = tmap.getTileChar(xtile,  ytile);
		
		// If it's not a dot (empty space) handle it
		if (ch != '.') {
			// Here we just stop the sprite
			s.stop();
			
			// You should move the sprite to a position that is not colliding
		}
				
//		if (player.getY() + player.getHeight() > tmap.getPixelHeight()) {
//			player.setY(tmap.getPixelHeight() - player.getHeight());
//		}
//		
//		if (player.getX() + player.getWidth() > tmap.getPixelWidth()) {
//			player.setX(tmap.getPixelWidth() - player.getWidth());
//		}
		
		/*
		 * We need to consider the other corners of the sprite. The above looked at the top left position, let's
		 * look at the bottom left
		 */
		xtile = (int)(sx / tileWidth);
		ytile = (int)((sy + s.getHeight()) / tileHeight);
		ch = tmap.getTileChar(xtile, ytile);
		
		if (tmap.getTileChar(xtile, ytile) != 'a') {
			falling = true;
		}
		
		if (tmap.getTileChar(xtile, ytile) == 'a') {
			falling = false;
		}
		
		// If it's not an empty space
		if (ch != '.') {
			// Let's make the sprite bounce
			s.setVelocityY(-s.getVelocityY());	// Reverse the velocity
		}
	}
	
	/**
	 * A boolean to check if two sprites have collided
	 * @param s1	The first sprite
	 * @param s2	The second sprite
	 * @return		True if they collided, false otherwise
	 */
    public boolean boundingBoxCollision(Sprite s1, Sprite s2) {
        return ((s1.getX() + s1.getImage().getWidth(null) > s2.getX()) && 
                (s1.getX() < (s2.getX() + s2.getImage().getWidth(null))) &&
                ((s1.getY() + s1.getImage().getHeight(null) > s2.getY()) && 
                		(s1.getY() < s2.getY() + s2.getImage().getHeight(null))));
    }
    	
	/**
	 * Override of the {@link GameCore#keyPressed} event defined in {@link GameCore} to catch our own events
	 * @param e	The event that has been generated
	 */
	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();
		
		if (key == KeyEvent.VK_ESCAPE) stop();
		if (key == KeyEvent.VK_RIGHT) walkRight = true;
		if (key == KeyEvent.VK_LEFT) walkLeft = true; 
	}
	
	public void keyReleased(KeyEvent e) {
		int key = e.getKeyCode();
		
		// Switch statement instead of lots of if statements. Need to use break to prevent fall through
		switch (key) {
		case KeyEvent.VK_ESCAPE : stop(); break;
		case KeyEvent.VK_LEFT : case KeyEvent.VK_RIGHT : walkLeft = false; walkRight = false; break;
		default : break;
		}
	}
}