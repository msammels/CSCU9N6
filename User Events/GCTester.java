import java.awt.*;
import game2D.*;
import java.awt.event.*;
import java.awt.geom.*;

/**
 * CSCU9N6 - Practical 3
 * GameCore.java
 * 
 * GCTester demonstrates how we can override the GameCore class to create our own 'game'. We usually need to implement at
 * least the 'draw' and 'update' (not including any local event handling) to begin the process. You should also write your
 * own 'init' method that will initialise event handlers etc. By default GameCore will handle the 'Escape' key to quit
 * the game but you can override this with your own event handler
 * 
 * @author Michael Sammels
 * @version 11.02.2022
 */

public class GCTester extends GameCore {
    long total;         // Total time elapsed
    Animation anim;     // Our animation
    Sprite rock;        // Our sprite
    Boolean paused = false;
    private double angle = 45;
    
    /**
     * The obligatory main method that creates an instance of our GCTester class and starts it running
     * @param args 
     */
    public static void main(String[] args) {
        GCTester gct = new GCTester();
        gct.init();
        
        // Start in windowed mode with an 800x600 screen
        gct.run(false, 768, 576);
    }
    
    /**
     * Initialise the class, e.g., set up variables, animations, register event handlers
     */
    public void init() {
        total = 0;
        anim = new Animation();
        anim.addFrame(loadImage("images/rock.png"), 250);
        
        rock = new Sprite(anim);
        rock.setPosition(70, 70);
        rock.setVelocity(0.1f, 0.1f);
        
        angle = 90;
    }
    
    /**
     * Draw the current frame
     * @param g 
     */
    @Override
    public void draw(Graphics2D g) {
        // A simple demo - note that is not very efficient since we fill the screen on every frame
        g.setColor(Color.black);
        g.fillRect(0, 0, getWidth(), getHeight());
        g.setColor(Color.yellow);
        g.drawString("Time Expired: " + total, 30, 50);
        
        AffineTransform transform = new AffineTransform();
        transform.translate(Math.round(rock.getX()), Math.round(rock.getY()));
        transform.scale(2.5f, 2.5f);
        
        int width = rock.getImage().getWidth(null);
        int height = rock.getImage().getHeight(null);
        transform.rotate(Math.toRadians(angle), width / 2, height / 2);
        g.drawImage(rock.getImage(), transform, null);
    }
    
    /**
     * Update any sprites and check for collisions
     * @param elapsed 
     */
    @Override
    public void update(long elapsed) {
        if (paused) return;
        total += elapsed;
        rock.update(elapsed);
        
        AffineTransform transform = new AffineTransform();
        transform.translate(Math.round(rock.getX()), Math.round(rock.getY()));
        transform.scale(2.5f, 2.5f);
        transform.rotate(Math.toRadians(angle = (angle + 5) % 360));
        
        checkScreenEdge(rock);
    }
    
    /**
     * The event handlers for key press detection
     * @param e 
     */
    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_ESCAPE -> stop();                      // Stop game loop
            case KeyEvent.VK_S      -> rock.stop();                 // Stop the rock
            case KeyEvent.VK_G      -> paused = !paused;            // Pause and unpause the game loop
            case KeyEvent.VK_RIGHT  -> angle = (angle + 5) % 360;   // Rotate right
            case KeyEvent.VK_LEFT   -> angle = (angle - 5) % 360;   // Rotate left
            
            // Unused key event
            default -> {}
        }
        e.consume();
    }
    
    /**
     * Basic collision detection
     * @param s 
     */
    public void checkScreenEdge(Sprite s) {
        if (s.getX() > getWidth())  { s.setX(0); }
        if (s.getY() > getHeight()) { s.setY(0); }
        
        if (s.getX() > getHeight()) { s.setX(0); }
        if (s.getY() > getWidth())  { s.setY(0); }
    }
}