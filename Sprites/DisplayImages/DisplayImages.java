import java.awt.Graphics;
import java.awt.Image;
import javax.swing.*;

/**
 * CSCU9N6 - Practical 1
 * DisplayImages.java
 * 
 * @author Michael Sammels
 * @version 01.02.2022
 */

public class DisplayImages extends JFrame {
    private Image background;
    private Image pic;
    
    public static void main(String[] args) {
        DisplayImages sf = new DisplayImages();
        sf.go();
    }
    
    public void go() {
        background = new ImageIcon("images/background.jpg").getImage();
        pic = new ImageIcon("images/translucent.png").getImage();
        
        setSize(800, 600);
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
    
    public void paint(Graphics g) {
        g.drawImage(background, 0, 0, null);
        g.drawImage(pic, 20, 20, null);
        g.drawImage(pic, 120, 120, null);
        
        g.drawImage(pic, 320, 320, null);
        g.drawImage(pic, 420, 420, null);
    }
}
