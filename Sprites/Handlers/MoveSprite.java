import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.event.*;
import javax.swing.ImageIcon;
import javax.swing.JFrame;

/**
 * CSCU9N6 - Practical 2
 * MoveSprite.java
 * 
 * @author Michael Sammels
 * @version 01.02.2022
 */

public class MoveSprite extends JFrame implements KeyListener, MouseListener {
    /**
     * Our main method
     * @param args 
     */
    public static void main(String args[]) {
        MoveSprite ms = new MoveSprite();
        ms.go();
    }

    private Image bgImage;
    private Sprite sprite;
    private boolean stop;

    /**
     * Setting up the assets
     */
    public void loadImages() {
        // Load images
        Image player1 = loadImage("images/player1.png");
        Image player2 = loadImage("images/player2.png");
        Image player3 = loadImage("images/player3.png");

        // Create sprite
        Animation anim = new Animation();
        anim.addFrame(player1, 250);
        anim.addFrame(player2, 150);
        anim.addFrame(player1, 150);
        anim.addFrame(player2, 150);
        anim.addFrame(player3, 200);
        anim.addFrame(player2, 150);

        sprite = new Sprite(anim);
        
        // Start the sprite off moving down and to the right
        sprite.setPosition(50, 50);
        sprite.setVelocityX(0.0f);
        sprite.setVelocityY(0.0f);
    }

    /**
     * Loading in the assets
     * @param fileName
     * @return 
     */
    private Image loadImage(String fileName) {
        return new ImageIcon(fileName).getImage();
    }

    /**
     * Starting the main loop
     */
    public void go() {
        setSize(800, 600);
        setVisible(true);
        addKeyListener(this);
        addMouseListener(this);
        loadImages();
        animationLoop();
    }

    /**
     * This where our graphics logic lives
     */
    public void animationLoop() {
        long startTime = System.currentTimeMillis();
        long currTime = startTime;
        
        /*
        We are going to use an image buffer to make the draw process more efficient. This buffer will be the same size
        as the screen
        */
        BufferedImage buffer;
        buffer = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_RGB);
        
        // We can get a virtual graphics object from our buffer which we can draw to
        Graphics2D bg = (Graphics2D) buffer.createGraphics();

        while (!stop) {
            long elapsedTime = System.currentTimeMillis() - currTime;
            currTime += elapsedTime;

            // Call a general purpose update method
            update(elapsedTime);

            // To avoid flickering, draw to an image buffer first,
            draw(bg);

            // Now draw the contents of this image buffer on the screen.
            Graphics g = getGraphics();
            g.drawImage(buffer, 0, 0, null);
            g.dispose();

            // take a nap
            try {
                Thread.sleep(20);
            } catch (InterruptedException ex) {}
        }
        System.exit(0);
    }

    /**
     * This is our general purpose update method where we will update our sprites and check for collisions. You should update
     * positions first and then fix any collisions after this since the next step is to draw the result and the user would
     * see any unresolved collisions
     * @param elapsedTime
     */
    public void update(long elapsedTime) {
        // Update the position of the sprite
        sprite.update(elapsedTime);

        // Check if the sprite has hit the bounds of the screen
        if (sprite.getX() < 0) {
            sprite.setVelocityX(Math.abs(sprite.getVelocityX()));
        } else if (sprite.getX() + sprite.getWidth() >= getWidth()) {
            sprite.setVelocityX(-Math.abs(sprite.getVelocityX()));
        }
        
        if (sprite.getY() < 0) {
            sprite.setVelocityY(Math.abs(sprite.getVelocityY()));
        } else if (sprite.getY() + sprite.getHeight() >= getHeight()) {
            sprite.setVelocityY(-Math.abs(sprite.getVelocityY()));
        }
    }

    /**
     * Drawing our sprites to the screen
     * @param g 
     */
    public void draw(Graphics g) {
        // Draw background
        //g.drawImage(bgImage, 0, 0, null);

        // Fills background with current foreground colour
        g.fillRect(0, 0, getWidth(), getHeight());

        // Draw sprite
        g.drawImage(sprite.getImage(), Math.round(sprite.getX()), Math.round(sprite.getY()), null);
    }

    /**
     * We can control our sprites using the arrows keys on the keyboard, as well as the Esc key
     * @param e 
     */
    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_ESCAPE -> stop = true;                                         // Exit the program
            case KeyEvent.VK_LEFT   -> sprite.setVelocityX(sprite.getVelocityX() - 0.1f);   // Move left and slow down
            case KeyEvent.VK_RIGHT  -> sprite.setVelocityX(sprite.getVelocityX() + 0.1f);   // Move right and speed up
            case KeyEvent.VK_UP     -> sprite.setVelocityY(sprite.getVelocityY() - 0.1f);   // Move up and speed up
            case KeyEvent.VK_DOWN   -> sprite.setVelocityY(sprite.getVelocityY() + 0.1f);   // Move down and slow down
            
            // Unused key event
            default -> {}
        }
        e.consume();
    }

    /**
     * Quite simple here: when the key being pressed is released, we stop the sprite
     * @param e 
     */
    @Override
    public void keyReleased(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_LEFT   -> sprite.setVelocityX(0.0f);
            case KeyEvent.VK_RIGHT  -> sprite.setVelocityX(0.0f);
            case KeyEvent.VK_UP     -> sprite.setVelocityY(0.0f);
            case KeyEvent.VK_DOWN   -> sprite.setVelocityY(0.0f);
        }
        e.consume();
    }

    /**
     * Consumes this event so that it will not be processed in the default manner by the source which originated it
     * @param e 
     */
    @Override
    public void keyTyped(KeyEvent e) {
        e.consume();
    }
    
    /**
     * Moving the sprite to the location of the mouse
     * @param e 
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        sprite.setPosition(e.getX(), e.getY());
    }

    /**
     * Not implemented
     * @param e 
     */
    @Override
    public void mousePressed(MouseEvent e) {}

    /**
     * Not implemented
     * @param e 
     */
    @Override
    public void mouseReleased(MouseEvent e) {}

    /**
     * Not implemented
     * @param e 
     */
    @Override
    public void mouseEntered(MouseEvent e) {}

    /**
     * Not implemented
     * @param e 
     */
    @Override
    public void mouseExited(MouseEvent e) {}
}
