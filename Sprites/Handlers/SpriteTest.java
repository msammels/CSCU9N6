import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Arrays;
import javax.swing.ImageIcon;
import javax.swing.JFrame;

/**
 * CSCU9N6 - Practical 2
 * SpriteTest.java
 * 
 * @author Michael Sammels
 * @version 01.02.2022
 */

public class SpriteTest extends JFrame {
    /**
     * The main launcher
     * @param args 
     */
    public static void main(String args[]) {
        SpriteTest test = new SpriteTest();
        test.go();
    }
    
    private Sprite sprite, sprite1, sprite2;
    public ArrayList<Sprite> sprites;
    
    /**
     * Loading the assets
     */
    public void loadImages() {
        // Load images
        Image player1 = loadImage("images/player1.png");
        Image player2 = loadImage("images/player2.png");
        Image player3 = loadImage("images/player3.png");

        // Create sprite
        Animation anim = new Animation();
        anim.addFrame(player1, 250);
        anim.addFrame(player2, 150);
        anim.addFrame(player1, 150);
        anim.addFrame(player2, 150);
        anim.addFrame(player3, 200);
        anim.addFrame(player2, 150);
        
        // Create another sprite
        Animation anim1 = new Animation();
        anim1.addFrame(player1, 240);
        anim1.addFrame(player2, 140);
        anim1.addFrame(player1, 140);
        anim1.addFrame(player2, 140);
        anim1.addFrame(player3, 190);
        anim1.addFrame(player2, 140);
        
        // Let's have three for testing purposes - but this will be the reverse of the first sprite
        Animation anim2 = new Animation();
        anim2.addFrame(player1, 230);
        anim2.addFrame(player2, 130);
        anim2.addFrame(player1, 130);
        anim2.addFrame(player2, 130);
        anim2.addFrame(player3, 180);
        anim2.addFrame(player2, 130);

        sprite = new Sprite(anim);
        sprite1 = new Sprite(anim1);
        sprite2 = new Sprite(anim2);
        
        // Start the sprite off moving down and to the right
        sprite.setVelocityX(0.2f);
        sprite.setVelocityY(0.2f);
        
        sprite1.setVelocityX(0.3f);
        sprite1.setVelocityY(0.3f);
        
        // As with the animation frames above, we reversing the sprites velocity as well
        sprite2.setVelocityX(0.4f);
        sprite2.setVelocityY(0.4f);
        
        // Create an array list to store the sprites - this cuts down on the amount of code we use
        sprites = new ArrayList<>(Arrays.asList(sprite, sprite1, sprite2));
    }

    /**
     * This allows us to load our sprites and other images in
     * @param fileName
     * @return 
     */
    private Image loadImage(String fileName) {
        return new ImageIcon(fileName).getImage();
    }

    /**
     * This starts the loop
     */
    public void go() {
        setSize(800, 600);
        setVisible(true);
        loadImages();
        animationLoop();
    }

    /**
     * Our main animation loop
     */
    public void animationLoop() {
        long startTime = System.currentTimeMillis();
        long currTime = startTime;

        /*
        We are going to use an image buffer to make the draw process more efficent. This buffer will be the same size
        as the screen
        */
        BufferedImage buffer;
        buffer = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_RGB);
        
        // We can get a virtual graphics object from our buffer which we can draw to
        Graphics2D bg = (Graphics2D) buffer.createGraphics();

        while (true) {
            long elapsedTime = System.currentTimeMillis() - currTime;
            currTime += elapsedTime;

            // Call a general purpose update method
            update(elapsedTime);

            // To avoid flickering, draw to an image buffer first,
            draw(bg);

            // Now draw the contents of this image buffer on the screen.
            var g = getGraphics();
            g.drawImage(buffer, 0, 0, null);
            g.dispose();

            // Take a nap
            try {
                Thread.sleep(20);
            } catch (InterruptedException ex) {}
        }
    }

    /**
     * This is our general purpose update method where we will update our sprites and check for collisions. You should update
     * positions first and then fix any collisions after this since the next step is to draw the result and the user would
     * see any unresolved collisions
     * @param elapsedTime
     */
    public void update(long elapsedTime) {
        for (Sprite sprite : sprites) {
            // Check if the sprite has hit the bounds of the screen
            if (sprite.getX() < 0) {
                sprite.setVelocityX(Math.abs(sprite.getVelocityX()));
            } else if (sprite.getX() + sprite.getWidth() >= getWidth()) {
                sprite.setVelocityX(-Math.abs(sprite.getVelocityX()));
            }
            
            if (sprite.getY() < 0) {
                sprite.setVelocityY(Math.abs(sprite.getVelocityY()));
            } else if (sprite.getY() + sprite.getHeight() >= getHeight()) {
                sprite.setVelocityY(-Math.abs(sprite.getVelocityY()));
            }
            
            // Update the position of the sprites
            sprite.update(elapsedTime);
        }
    }
    
    /**
     * Drawing the sprites to the screen
     * @param g 
     */
    public void draw(Graphics g) {
        // Draw background
        //g.drawImage(bgImage, 0, 0, null);

        // Fills background with current foreground colour
        g.fillRect(0, 0, getWidth(), getHeight());
        
        // Draw the sprites
        for (Sprite sprite: sprites) {
            g.drawImage(sprite.getImage(), Math.round(sprite.getX()), Math.round(sprite.getY()), null);
        }
    }
}
